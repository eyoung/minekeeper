
// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


mod minekeeper;

extern crate rustc_serialize;
#[macro_use]
extern crate rustful;
extern crate regex;

use minekeeper::{MinekeeperConfig, MinekeeperError, MinecraftInstance, MinekeeperMessage, ApiResponse, MessageContent};
use minekeeper::minekeeper_constants::config_path;
use std::fs;
use std::collections::HashMap;
use std::sync::mpsc::{Sender, Receiver, channel};
use std::sync::{Arc, Mutex};
use std::thread;
use rustful::{Server, Handler, Context, Response, TreeRouter, StatusCode};
use rustc_serialize::json;

fn main() {
    let configs = load_configs();
    let mut minecraft_instances = HashMap::new();
    match configs {
        Ok(config_vec) => {
            for config in config_vec {
                minecraft_instances.insert(config.server_name.to_string(), MinecraftInstance::setup_instance(config));
            }
        }
        Err(error) => println!("{}", error)
    }

    let (url_dispatch_sender, url_dispatch_receiver) = channel();
    let url_dispatch_mux = Arc::new(Mutex::new(url_dispatch_sender));
    
    thread::spawn(move || {
        let router = insert_routes! {
            TreeRouter::new() => {
                "servers" => {
                    Get: ApiRequest::ListServersRequest(url_dispatch_mux.clone()),
                    ":server" => {
                        Get: ApiRequest::StatusRequest(url_dispatch_mux.clone()),
                        "players" => {
                            Get: ApiRequest::PlayersRequest(url_dispatch_mux.clone())
                        },

                        "start" => {
                            Get: ApiRequest::StartRequest(url_dispatch_mux.clone())
                        },

                        "stop" => {
                            Get: ApiRequest::StopRequest(url_dispatch_mux.clone())
                        },

                        "say" => {
                            Post: ApiRequest::SayRequest(url_dispatch_mux.clone())
                        }
                    }
                },

                "stopall" => {
                    Get: ApiRequest::StopAllRequest(url_dispatch_mux.clone())
                }
            }
        };

        let server = Server {
            handlers: router,
            host: 8080.into(),
            ..Server::default()
        };

        if let Err(_) = server.run() {
            println!("Error starting REST Service");
        }
    });

    let mut running = true;

    while running {
        let request = url_dispatch_receiver.recv().unwrap();
        match request {
            InstanceRequest::ListRequest(sender) => {
                let servers: Vec<&String> = minecraft_instances.keys().collect();
                let response_string = json::encode(&servers).unwrap();
                sender.send(ApiResponse::new(StatusCode::Ok, Some(response_string))).unwrap();
            }

            InstanceRequest::StatusRequest(instance, response_sender) => {
                match minecraft_instances.get(&instance) {
                    Some(instance_sender) => {
                        instance_sender.send(MinekeeperMessage::StatusRequest(response_sender)).unwrap();
                    }

                    None => {
                        response_sender.send(ApiResponse::new(StatusCode::NotFound, None)).unwrap();
                    }
                }
            }

            InstanceRequest::PlayersRequest(instance, name_filter, uuid_filter, whitelist_filter, op_filter, online_filter, response_sender) => {
                match minecraft_instances.get(&instance) {
                    Some(instance_sender) => {
                        instance_sender.send(MinekeeperMessage::GetPlayersRequest(name_filter, uuid_filter, whitelist_filter, op_filter, online_filter, response_sender)).unwrap();
                    }

                    None => {
                        response_sender.send(ApiResponse::new(StatusCode::NotFound, None)).unwrap();
                    }
                }
            }

            InstanceRequest::StartRequest(instance, response_sender) => {
                match minecraft_instances.get(&instance) {
                    Some(instance_sender) => {
                        instance_sender.send(MinekeeperMessage::StartRequest).unwrap();
                        response_sender.send(ApiResponse::new(StatusCode::Ok, None)).unwrap();
                    }

                    None => {
                        response_sender.send(ApiResponse::new(StatusCode::NotFound, None)).unwrap();
                    }
                }
            }

            InstanceRequest::StopRequest(instance, response_sender) => {
                match minecraft_instances.get(&instance) {
                    Some(instance_sender) => {
                        instance_sender.send(MinekeeperMessage::StopRequest(None)).unwrap();
                        response_sender.send(ApiResponse::new(StatusCode::Ok, None)).unwrap();
                    }

                    None => {
                        response_sender.send(ApiResponse::new(StatusCode::NotFound, None)).unwrap();
                    }
                }
            }

            InstanceRequest::StopAllRequest(response_sender) => {
                let (shutdown_sender, shutdown_receiver) = channel();
                for instance_sender in minecraft_instances.values() {
                    instance_sender.send(MinekeeperMessage::StopRequest(Some(shutdown_sender.clone()))).unwrap();
                }

                for _ in minecraft_instances.keys() {
                    shutdown_receiver.recv().unwrap();
                }

                response_sender.send(ApiResponse::new(StatusCode::Ok, None)).unwrap();
                running = false;
            }

            InstanceRequest::SayRequest(instance, message, response_sender) => {
                match minecraft_instances.get(&instance) {
                    Some(instance_sender) => {
                        instance_sender.send(MinekeeperMessage::SayRequest(message)).unwrap();
                        response_sender.send(ApiResponse::new(StatusCode::Ok, None)).unwrap();
                    }

                    None => response_sender.send(ApiResponse::new(StatusCode::NotFound, None)).unwrap()
                }
            }
        }
    }
}

fn load_configs() -> Result< Vec<MinekeeperConfig>, MinekeeperError> {
    let mut configs: Vec<MinekeeperConfig> = Vec::new();
    for dir_result in try!(fs::read_dir(config_path())) {
        let config_path = try!(dir_result);
        let config_path_string = config_path.path().to_string_lossy().into_owned();
        match MinekeeperConfig::config_from_file(&config_path_string) {
            Ok(config) => configs.push(config),
            Err(error) => println!("Error parsing config file {} : {}", &config_path_string, error)
        }
    }
    Ok(configs)
}

enum InstanceRequest {
    StatusRequest(String, Sender<ApiResponse>),
    StartRequest(String, Sender<ApiResponse>),
    StopRequest(String, Sender<ApiResponse>),
    SayRequest(String, String, Sender<ApiResponse>),
    // MotdRequest,
    // SayRequest,
    // OpRequest,
    PlayersRequest(String, Option<String>, Option<String>, Option<bool>, Option<bool>, Option<bool>, Sender<ApiResponse>),
    //WhitelistRequest,
    ListRequest(Sender<ApiResponse>),
    StopAllRequest(Sender<ApiResponse>)
}

enum ApiRequest {
    ListServersRequest(Arc<Mutex<Sender<InstanceRequest>>>),
    StatusRequest(Arc<Mutex<Sender<InstanceRequest>>>),
    PlayersRequest(Arc<Mutex<Sender<InstanceRequest>>>),
    StartRequest(Arc<Mutex<Sender<InstanceRequest>>>),
    StopRequest(Arc<Mutex<Sender<InstanceRequest>>>),
    StopAllRequest(Arc<Mutex<Sender<InstanceRequest>>>),
    SayRequest(Arc<Mutex<Sender<InstanceRequest>>>)
}

impl ApiRequest {
    fn dispatch_request(&self, api_request: InstanceRequest, response_channel_mux: &Arc<Mutex<Sender<InstanceRequest>>>) -> Result<(), MinekeeperError> {
        let response_channel_mux = response_channel_mux.clone();
        let response_channel = try!(response_channel_mux.lock().map_err(|_| {
            MinekeeperError::ServerError
        }));
        try!(response_channel.send(api_request));
        Ok(())
    }
}

impl Handler for ApiRequest {
    fn handle_request(&self, mut context: Context, mut response: Response) {
        let (request_sender, request_receiver) = channel();
        let api_request_result = match *self {
            ApiRequest::ListServersRequest(ref response_channel_mux) => {
                self.dispatch_request(InstanceRequest::ListRequest(request_sender.clone()), response_channel_mux)
            }

            ApiRequest::StatusRequest(ref response_channel_mux) => {
                if let Some(instance) = context.variables.get("server") {
                    self.dispatch_request(InstanceRequest::StatusRequest(instance.into_owned(), request_sender.clone()), response_channel_mux)
                } else {
                    Err(MinekeeperError::ServerError)
                }
            }

            ApiRequest::PlayersRequest(ref response_channel_mux) => {
                if let Some(instance) = context.variables.get("server") {
                    let name_filter: Option<String> = context.query.parse("name").ok();
                    let uuid_filter: Option<String> = context.query.parse("uuid").ok();
                    let whitelist_filter: Option<bool> = context.query.parse("white_listed").ok();
                    let op_filter: Option<bool> = context.query.parse("op").ok();
                    let online_filter: Option<bool> = context.query.parse("online").ok();
                    self.dispatch_request(InstanceRequest::PlayersRequest(instance.into_owned(), name_filter, uuid_filter, whitelist_filter, op_filter, online_filter, request_sender.clone()), response_channel_mux)
                } else {
                    Err(MinekeeperError::ServerError)
                }
            }

            ApiRequest::StartRequest(ref response_channel_mux) => {
                if let Some(instance) = context.variables.get("server") {
                    self.dispatch_request(InstanceRequest::StartRequest(instance.into_owned(), request_sender.clone()), response_channel_mux)
                } else {
                    Err(MinekeeperError::ServerError)
                }
            }

            ApiRequest::StopRequest(ref response_channel_mux) => {
                if let Some(instance) = context.variables.get("server") {
                    self.dispatch_request(InstanceRequest::StopRequest(instance.into_owned(), request_sender.clone()), response_channel_mux)
                } else {
                    Err(MinekeeperError::ServerError)
                }
            }

            ApiRequest::StopAllRequest(ref response_channel_mux) => {
                self.dispatch_request(InstanceRequest::StopAllRequest(request_sender.clone()), response_channel_mux)
            }

            ApiRequest::SayRequest(ref response_channel_mux) => {
                if let Some(instance) = context.variables.get("server") {
                    let body_result: json::DecodeResult<MessageContent> = context.body.decode_json_body();
                    match body_result {
                        Ok(content) => {
                            self.dispatch_request(InstanceRequest::SayRequest(instance.into_owned(), content.message, request_sender.clone()), response_channel_mux)
                        }

                        Err(e) => {
                            println!("Error parsing content body {:?}", e);
                            Err(MinekeeperError::RequestError)
                        }
                    }
                } else {
                    Err(MinekeeperError::ServerError)
                }
            }
        };

        if let Err(error) = api_request_result {
            match error {
                MinekeeperError::RequestError => request_sender.send(ApiResponse::new(StatusCode::BadRequest, None)).unwrap(),
                _ => request_sender.send(ApiResponse::new(StatusCode::InternalServerError, None)).unwrap()
            }
        }

        let api_response = match request_receiver.recv() {
            Ok(response) => response,
            Err(_) => {
                ApiResponse::new(StatusCode::InternalServerError, None)
            }
        };

        response.set_status(api_response.status_code);

        if let Some(body_content) = api_response.body_content {
            response.send(format!("{}\n", body_content));
        }
    }
}

