
// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use rustc_serialize::json;
use std::fs::File;
use std::io::{Read, Write};
use std::error::Error;
use minekeeper::MinekeeperError;
use minekeeper::minekeeper_constants::config_path;

#[derive(RustcDecodable, RustcEncodable)]
pub struct MinekeeperConfig {
    pub working_directory: String,
    pub server_name: String,
    pub minecraft_binary: String,
    pub auto_start: bool,
    pub motd: String
}

impl MinekeeperConfig {
    pub fn config_from_file(filename: &str) -> Result<MinekeeperConfig, MinekeeperError > {
        let mut file_content = String::new();
        let mut config_file = try!(File::open(filename));
        try!(config_file.read_to_string(&mut file_content));
        let config_data = try!(json::decode(&file_content));
        Ok(config_data)
    }

    pub fn save(&self) {
        let filename = format!("{}/{}.json", config_path(), self.server_name);
        match File::create(filename) {
            Ok(mut file) => {
                match json::encode(self) {
                    Ok(content) => {
                        if let Err(e) = file.write_all(content.as_bytes()) {
                            println!("Minekeeper::{} {:?}", self.server_name, e);
                        }
                    },
                    Err(e) => println!("Minekeeper::{} {:?}", self.server_name, e),
                }
            },
            Err(e) => println!("Minekeeper::{} {:?}", self.server_name, e),
        }
    }
}
