// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::process::{Command, Child, ChildStdin, ChildStdout, Stdio};
use minekeeper::{MinekeeperMessage, StatusResponse, PlayerCache, MinecraftPlayer, ApiResponse, MinekeeperConfig};
use std::sync::mpsc::{Sender, channel};
use std::thread;
use std::sync::Arc;
use std::io::{BufReader, BufRead, Write, Read};
use regex::Regex;
use rustc_serialize::json;
use rustful::StatusCode;

pub struct MinecraftInstance {
    pub server_name: Arc<String>,
    pub running: bool,
    pub player_cache: PlayerCache,
    pub configuration: MinekeeperConfig,

    retry_count: i16,
    instance_process: Option<Child>,
    instance_stdin: Option<ChildStdin>,
    instance_stdout: Option<BufReader<ChildStdout>>,
}

impl MinecraftInstance {
    fn new(config: MinekeeperConfig) -> MinecraftInstance {
        MinecraftInstance {
            server_name: Arc::new(config.server_name.to_string()),
            running: false,
            player_cache: PlayerCache::new(),
            retry_count: 0,
            instance_process: None,
            instance_stdin: None,
            instance_stdout: None,
            configuration: config,
        }
    }
    
    pub fn start_instance(&mut self) {
        match self.instance_process {
            Some(_) => println!("Instance already started"),
            None => {
                let mut p = Command::new("java");
                p.arg("-jar").arg("-d64").arg("-Xmx2g").arg("-Xms2G").arg(&self.configuration.minecraft_binary).arg("nogui");
                p.stdin(Stdio::piped());
                p.stdout(Stdio::piped());
                p.current_dir(&self.configuration.working_directory);
                match p.spawn() {
                    Ok(mut child_process) => {
                            if let Some(inst_stdin) = child_process.stdin.take() {
                                self.instance_stdin = Some(inst_stdin);
                            }
                            if let Some(inst_stdout) = child_process.stdout.take() {
                                self.instance_stdout = Some(BufReader::new(inst_stdout));
                            }
                            self.instance_process = Some(child_process);
                            self.running = true;
                    }
                    Err(e) => println!("Error spawning minecraft instance {:?}", e),
                }
            }
        }
    }

    pub fn reset_instance(&mut self) {
        if self.instance_process.is_some() {
            self.instance_process = None;
            self.instance_stdin = None;
            self.running = false;
            self.player_cache.reset()
        }
    }

    fn watch_instance(&mut self, channel: &Sender<MinekeeperMessage>) {
        match self.instance_stdout.take() {
            Some(mut minecraft_stdout) => {
                let server_name = self.server_name.clone();
                let channel = channel.clone();
                let warn_regex = Regex::new(r"^(\[\d{2}:\d{2}:\d{2}\]) \[Server thread/WARN\].*").unwrap();
                let motd_regex = Regex::new(r"^(\[\d{2}:\d{2}:\d{2}\]) \[Server thread/INFO\]: <.+> #MOTD (.+)").unwrap();
                let login_regex = Regex::new(r"^(\[\d{2}:\d{2}:\d{2}\]) \[Server thread/INFO\]: ([A-Za-z_0-9]+) joined the game").unwrap();
                let logout_regex = Regex::new(r"^(\[\d{2}:\d{2}:\d{2}\]) \[Server thread/INFO\]: ([A-Za-z_0-9]+) left the game").unwrap();
                let player_discovery_regex = Regex::new(r"^(\[\d{2}:\d{2}:\d{2}\]) \[User Authenticator #1/INFO\]: UUID of player ([A-Za-z_0-9]+) is ([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})").unwrap();

                thread::spawn( move || {
                    let mut line_buffer = String::new();

                    let mut valid_pipe = true;
                    while valid_pipe == true {
                        match minecraft_stdout.read_line(&mut line_buffer) {
                            Ok(bytes) => {
                                match line_buffer {
                                    ref message if motd_regex.is_match(&line_buffer) => {
                                        if let Some(captures) = motd_regex.captures(message) {
                                            if let Some(text) = captures.at(2) {
                                                channel.send(MinekeeperMessage::UpdateMotdRequest(text.to_string(), None)).is_ok();
                                            }
                                        }
                                    },

                                    ref message if login_regex.is_match(&line_buffer) => {
                                        if let Some(login) = login_regex.captures(message) {
                                            if let Some(player) = login.at(2) {
                                                channel.send(MinekeeperMessage::LoginMessage(player.to_string())).is_ok();
                                            }
                                        }
                                    },

                                    ref message if logout_regex.is_match(&line_buffer) => {
                                        if let Some(logout) = logout_regex.captures(message) {
                                            if let Some(player) = logout.at(2) {
                                                channel.send(MinekeeperMessage::LogoutMessage(player.to_string())).is_ok();
                                            }
                                        }
                                    },

                                    ref message if player_discovery_regex.is_match(&line_buffer) => {
                                        if let Some(discovery) = player_discovery_regex.captures(message) {
                                            if let Some(player) = discovery.at(2) {
                                                if let Some(uuid) = discovery.at(3) {
                                                    channel.send(MinekeeperMessage::DiscoveryMessage(player.to_string(), uuid.to_string())).is_ok();
                                                }
                                            }
                                        }
                                    },

                                    _ => {},
                                }
                                if bytes > 0 {
                                    if !warn_regex.is_match(&line_buffer) {
                                        print!("Minekeeper::{} {}", server_name, line_buffer);
                                    }
                                    line_buffer.clear();
                                } else {
                                    valid_pipe = false;
                                    println!("Minekeeper::{} pipe closed", server_name);
                                }
                            }
                            Err(error) => {
                                valid_pipe = false;
                                println!("Minekeeper::{} Minecraft stdout encountered an error {:?}", server_name, error);
                            }
                        }
                    }
                    channel.send(MinekeeperMessage::ServerInstanceClosed).unwrap();
                });
            },

            None => println!("{}::Error: minecraft instance stdout does not exist", self.server_name),
        }
    }

    pub fn setup_instance(config: MinekeeperConfig)  -> Sender<MinekeeperMessage> {
        let (spawn_sender, spawn_receiver) = channel();
        let spawn_sender = spawn_sender.clone();
 
        thread::spawn(move || {
            let (sender, receiver) = channel();
            
            let mut instance = MinecraftInstance::new(config);
            instance.player_cache.read_whitelist(&instance.configuration.working_directory);
            instance.player_cache.read_ops(&instance.configuration.working_directory);
            if instance.configuration.auto_start {
                instance.start_instance();
                instance.watch_instance(&sender);
            }
            if let Ok(_) = spawn_sender.send(sender.clone()) {
                println!("start message dispatch loop");
                while let Ok(message) = receiver.recv() {
                    match message {
                        MinekeeperMessage::LoginMessage(name) => {
                            if instance.running {
                                if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                    writeln!(instance_stdin, "tellraw @a {{\"text\":\"Welcome {}\", \"color\":\"aqua\"}}", name);
                                    if !instance.configuration.motd.is_empty() {
                                        writeln!(instance_stdin, "tellraw {} {{\"text\":\"#### Message of the day ####\", \"color\":\"green\"}}", name);
                                        writeln!(instance_stdin, "tellraw {} {{\"text\":\"{}\", \"color\":\"green\"}}", name, instance.configuration.motd);
                                    }
                                }
                                instance.player_cache.set_online(&name);
                            }
                        },

                        MinekeeperMessage::LogoutMessage(name) => {
                            if instance.running {
                                instance.player_cache.set_offline(&name);
                            }
                        },

                        MinekeeperMessage::StatusRequest(response_channel) => {
                            let status_response = StatusResponse::new(&instance);
                            let body_content = json::encode(&status_response).unwrap();
                            response_channel.send(ApiResponse::new(StatusCode::Ok, Some(body_content))).unwrap();
                        },

                        MinekeeperMessage::StartRequest => {
                            if !instance.running {
                                instance.start_instance();
                                if instance.running {
                                    instance.watch_instance(&sender);
                                }
                            }
                        },

                        MinekeeperMessage::StopRequest(return_sender) => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "tellraw @a {{\"text\":\"Server will shut down in 10 seconds.\", \"color\":\"red\"}}");
                                writeln!(instance_stdin, "tellraw @a {{\"text\":\"Sorry for the trouble.\", \"color\":\"red\"}}");

                                let message_callback = sender.clone();
                                thread::spawn(move || {
                                    thread::sleep_ms(10000);
                                    message_callback.send(MinekeeperMessage::StopDelayFinished).unwrap();
                                    if let Some(exit_callback) = return_sender {
                                        exit_callback.send(MinekeeperMessage::StopDelayFinished).unwrap();
                                    }
                                });
                            } else {
                                if let Some(exit_callback) = return_sender {
                                    exit_callback.send(MinekeeperMessage::StopDelayFinished).unwrap();
                                }
                            }
                        },

                        MinekeeperMessage::StopDelayFinished => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "stop");
                            }
                            instance.reset_instance();
                        },

                        MinekeeperMessage::ServerInstanceClosed => {
                            if instance.running && instance.retry_count < 3 {
                                instance.retry_count = instance.retry_count+1;
                                instance.reset_instance();
                                instance.start_instance();
                                instance.watch_instance(&sender);
                                let timer_callback = sender.clone();
                                thread::spawn( move || {
                                    thread::sleep_ms(60000);
                                    timer_callback.send(MinekeeperMessage::RetryTimerTriggered);
                                });
                            } else {
                                instance.reset_instance();
                            }
                        },

                        MinekeeperMessage::RetryTimerTriggered => {
                            instance.retry_count = instance.retry_count - 1;
                        },

                        MinekeeperMessage::GetMotdRequest(return_sender) => {
                            return_sender.send(instance.configuration.motd.to_string());
                        },

                        MinekeeperMessage::UpdateMotdRequest(message, return_sender) => {
                            instance.configuration.motd = message;
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                if !instance.configuration.motd.is_empty() {
                                    writeln!(instance_stdin, "tellraw @a {{\"text\":\"#### Message of the day ####\", \"color\":\"green\"}}");
                                    writeln!(instance_stdin, "tellraw @a {{\"text\":\"{}\", \"color\":\"green\"}}", instance.configuration.motd);
                                }
                            }
                            instance.configuration.save();
                            if let Some(motd_response) = return_sender {
                                motd_response.send(instance.configuration.motd.to_string()).is_ok();
                            }
                        },

                        MinekeeperMessage::SayRequest(message) => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "tellraw @a {{\"text\":\"[Server Message] {}\", \"color\": \"green\"}}", message);
                            }
                        },

                        MinekeeperMessage::AddOpRequest(player, return_sender) => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "op {}", player);
                            }
                            instance.player_cache.set_op(&player, true);
                            return_sender.send(json::encode(&instance.player_cache.get_op_players()).unwrap()).is_ok();
                        },

                        MinekeeperMessage::RemoveOpRequest(player, return_sender) => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "deop {}", player);
                            }

                            instance.player_cache.set_op(&player, false);
                            return_sender.send(json::encode(&instance.player_cache.get_op_players()).unwrap()).is_ok();
                        },

                        MinekeeperMessage::GetOpRequest(return_sender) => {
                            return_sender.send(json::encode(&instance.player_cache.get_op_players()).unwrap()).is_ok();
                        },

                        MinekeeperMessage::GetPlayersRequest(name_filter, uuid_filter, whitelist_filter, op_filter, online_filter, return_sender) => {
                            let players = instance.player_cache.get_filtered_players(name_filter, uuid_filter, whitelist_filter, op_filter, online_filter);
                            let return_body = json::encode(&players).unwrap();
                            return_sender.send(ApiResponse::new(StatusCode::Ok, Some(return_body))).unwrap();
                        },

                        MinekeeperMessage::DiscoveryMessage(player, uuid) => {
                            let cached_player = instance.player_cache.get_cached_player(&player);
                            cached_player.uuid = uuid;
                        },

                        MinekeeperMessage::GetWhitelistRequest(return_sender) => {
                            let whitelisted_players = instance.player_cache.get_whitelist_players();
                            return_sender.send(json::encode(&whitelisted_players).unwrap()).is_ok();
                        },

                        MinekeeperMessage::AddWhitelistRequest(player, return_sender) => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "whitelist add {}", player);
                                instance.player_cache.get_cached_player(&player).white_listed = true;
                                return_sender.send(json::encode(&instance.player_cache.get_whitelist_players()).unwrap()).is_ok();
                            }
                        },

                        MinekeeperMessage::RemoveWhitelistRequest(player, return_sender) => {
                            if let Some(ref mut instance_stdin) = instance.instance_stdin {
                                writeln!(instance_stdin, "whitelist remove {}", player);
                                instance.player_cache.get_cached_player(&player).white_listed = false;
                                return_sender.send(json::encode(&instance.player_cache.get_whitelist_players()).unwrap()).is_ok();
                            }
                        },

                        _ => {},

                    }
                }
                println!("Exit message dispatch loop, channel closed");
            }
        });
        spawn_receiver.recv().unwrap()
    }
}
