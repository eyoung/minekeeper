
// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::error::Error;
use std::convert::From;
use std::fmt;
use std::io;
use std::sync::mpsc::SendError;
use rustc_serialize::json::DecoderError;
use InstanceRequest;

#[derive(Debug)]
pub enum MinekeeperError {
    ParseError(DecoderError),
    IOError(io::Error),
    OperationNotSupported,
    ServerError,
    RequestError
}

impl Error for MinekeeperError {
    fn description(&self) -> &str {
        match *self {
            MinekeeperError::ParseError(ref error)  => error.description(),
            MinekeeperError::IOError(ref error) => error.description(),
            MinekeeperError::OperationNotSupported => "This operation is not supported",
            MinekeeperError::ServerError => "A server error occured",
            MinekeeperError::RequestError => "Bad Request"
        }
    }
}

impl fmt::Display for MinekeeperError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            MinekeeperError::ParseError(ref error) => error.fmt(f),
            MinekeeperError::IOError(ref error) => error.fmt(f),
            MinekeeperError::OperationNotSupported => "This operation is not supported".fmt(f),
            MinekeeperError::ServerError => "A server error occured".fmt(f),
            MinekeeperError::RequestError => "Bad Request".fmt(f)
        }
    }
}

impl From<io::Error> for MinekeeperError {
    fn from(error: io::Error) -> MinekeeperError {
        MinekeeperError::IOError(error)
    }
}

impl From<DecoderError> for MinekeeperError {
    fn from(error: DecoderError) -> MinekeeperError {
        MinekeeperError::ParseError(error)
    }
}

impl From<SendError<InstanceRequest>> for MinekeeperError {
    fn from(error: SendError<InstanceRequest>) -> MinekeeperError {
        MinekeeperError::ServerError
    }
}
