
// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rustc_serialize::json;
use rustc_serialize::json::DecoderError;
use std::collections::HashMap;
use std::io::Read;
use std::fs::File;

#[derive(RustcDecodable, RustcEncodable, Debug)]
pub struct MojangPlayerFormat {
    uuid: String,
    name: String,
}



#[derive(RustcEncodable, Debug)]
pub struct MinecraftPlayer {
    pub uuid: String,
    pub name: String,
    pub white_listed: bool,
    pub op: bool,
    pub online: bool,
}

impl MinecraftPlayer {
    fn new(name: &str) -> MinecraftPlayer {
        MinecraftPlayer {
            uuid: "".to_string(),
            name: name.to_lowercase(),
            white_listed: false,
            online: false,
            op: false,
        }
    }
}

impl From<MojangPlayerFormat> for MinecraftPlayer {
    fn from(mj_player: MojangPlayerFormat) -> MinecraftPlayer {
        let mut new_player = MinecraftPlayer::new(&mj_player.name);
        new_player.uuid = mj_player.uuid;
        new_player
    }
}

#[derive(RustcEncodable, Debug)]
pub struct PlayerCache {
    players: HashMap<String, MinecraftPlayer>,
}

impl PlayerCache {

    pub fn new() -> PlayerCache {
        PlayerCache { players: HashMap::new() }
    }

    pub fn get_online_players(&self) -> Vec<&MinecraftPlayer> {
        self.players.values().filter(|&player| {
            player.online
        }).collect()
    }

    pub fn get_op_players(&self) -> Vec<&MinecraftPlayer> {
        self.players.values().filter(|&player| {
            player.op
        }).collect()
    }

    pub fn get_whitelist_players(&self) -> Vec<&MinecraftPlayer> {
        self.players.values().filter(|&player| {
            player.white_listed
        }).collect()
    }

    pub fn read_ops(&mut self, working_directory: &str) {
        let path = format!("{}/{}", working_directory, "ops.json");

        let mut file_content = String::new();

        match File::open(path) {
            Ok(mut op_file) =>  {
                match op_file.read_to_string(&mut file_content) {
                    Ok(_) => {
                        let decoded_players: Result<Vec<MojangPlayerFormat>, DecoderError> = json::decode(&file_content);
                        match decoded_players {
                            Ok(players_array) => {
                                for player in players_array {
                                    let cached_player = self.players.entry(player.name.to_lowercase()).or_insert(MinecraftPlayer::from(player));
                                    cached_player.op = true;
                                }

                            },

                            Err(error) => {
                                println!("Error decoding op file json: {:?}", error);
                            }
                        }
                    },

                    Err(error) => {
                        println!("Error reading from op file: {:?}", error);
                    }
                }
            },

            Err(error) => {
                println!("Error opening op file: {:?}", error);
            },
        }
    }

    pub fn read_whitelist(&mut self, working_directory: &str) {
        let path = format!("{}/{}", working_directory, "whitelist.json");

        let mut file_content = String::new();

        match File::open(path) {
            Ok(mut op_file) =>  {
                match op_file.read_to_string(&mut file_content) {
                    Ok(_) => {
                        let decoded_players: Result<Vec<MojangPlayerFormat>, DecoderError> = json::decode(&file_content);
                        match decoded_players {
                            Ok(players_array) => {
                                for player in players_array {
                                    let cached_player = self.players.entry(player.name.to_lowercase()).or_insert(MinecraftPlayer::from(player));
                                    cached_player.white_listed = true;
                                }

                            },

                            Err(error) => {
                                println!("Error decoding whitelist file json: {:?}", error);
                            }
                        }
                    },

                    Err(error) => {
                        println!("Error reading from whitelist file: {:?}", error);
                    }
                }
            },

            Err(error) => {
                println!("Error opening whitelist file: {:?}", error);
            },
        }
    }

    pub fn reset(&mut self) {
        for (_, player) in self.players.iter_mut() {
            player.online = false;
        }
    }

    pub fn set_online(&mut self, player_name: &str) {
        self.get_cached_player(player_name).online = true;
    }

    pub fn set_offline(&mut self, player_name: &str) {
        self.get_cached_player(player_name).online = false;
    }

    pub fn set_op(&mut self, player_name: &str, enable_op: bool) {
        self.get_cached_player(player_name).op = enable_op;
    }

    pub fn online_count(&self) -> usize {
        self.get_online_players().len()
    }

    pub fn get_all_players(&self) -> Vec<&MinecraftPlayer> {
        self.players.values().collect()
    }

    pub fn get_cached_player(&mut self, player_name: &str) -> &mut MinecraftPlayer {
        self.players.entry(player_name.to_lowercase()).or_insert(MinecraftPlayer::new(player_name))
    }

    pub fn get_filtered_players(&self, name: Option<String>, uuid: Option<String>, whitelisted: Option<bool>, op: Option<bool>, online: Option<bool>) -> Vec<&MinecraftPlayer> {
        self.players.values().filter(|&player| {
            match name.as_ref() {
                Some(name) => name.to_lowercase() == player.name,
                None => true
            }
        }).filter(|&player| {
            match uuid.as_ref() {
                Some(uuid) => uuid == &player.uuid,
                None => true
            }
        }).filter(|&player| {
            match whitelisted {
                Some(whitelisted) => whitelisted == player.white_listed,
                None => true
            }
        }).filter(|&player| {
            match op {
                Some(op) => op == player.op,
                None => true
            }
        }).filter(|&player| {
            match online {
                Some(online) => online == player.online,
                None => true
            }
        }).collect()
    }
}
