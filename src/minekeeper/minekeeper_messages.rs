
// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use minekeeper::{MinecraftInstance, MinecraftPlayer};
use std::sync::mpsc::Sender;
use rustful::StatusCode;

pub enum MinekeeperMessage {
    LoginMessage(String),
    LogoutMessage(String),
    StatusRequest(Sender<ApiResponse>),
    StartRequest,
    StopRequest(Option<Sender<MinekeeperMessage>>),
    StopDelayFinished,
    ServerInstanceClosed,
    RetryTimerTriggered,
    CloseMinekeeper,
    GetMotdRequest(Sender<String>),
    UpdateMotdRequest(String, Option<Sender<String>>),
    SayRequest(String),
    AddOpRequest(String, Sender<String>),
    RemoveOpRequest(String, Sender<String>),
    GetOpRequest(Sender<String>),
    GetPlayersRequest(Option<String>, Option<String>, Option<bool>, Option<bool>, Option<bool>, Sender<ApiResponse>),
    DiscoveryMessage(String, String),
    GetWhitelistRequest(Sender<String>),
    AddWhitelistRequest(String, Sender<String>),
    RemoveWhitelistRequest(String, Sender<String>),
}

#[derive(Debug, RustcEncodable)]
pub struct StatusResponse<'a> {
    server_name: String,
    running: bool,
    online_count: usize,
    players: Vec<&'a MinecraftPlayer>,
    motd: &'a str
}

impl <'a>StatusResponse<'a> {
    pub fn new(instance: &MinecraftInstance) -> StatusResponse {
        StatusResponse {
            server_name: instance.server_name.to_string(),
            running: instance.running,
            online_count: instance.player_cache.online_count(),
            players: instance.player_cache.get_all_players(),
            motd: &instance.configuration.motd
        }
    }
}

pub struct ApiResponse {
    pub status_code: StatusCode,
    pub body_content: Option<String>
}

impl ApiResponse {
    pub fn new(status_code: StatusCode, body_content: Option<String>) -> ApiResponse {
        ApiResponse {
            status_code: status_code,
            body_content: body_content
        }
    }
}

#[derive(RustcEncodable, RustcDecodable, Debug)]
pub struct MessageContent {
    pub message: String
}