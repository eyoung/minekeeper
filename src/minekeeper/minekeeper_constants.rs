// Minekeeper: Minecraft server management software
// Copyright (C) 2015  Eric Young <eyoung@madsciencesoftware.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

static CONFIG_PATH: &'static str = "/etc/minekeeper/configs";
static CONFIG_PATH_STAGING: &'static str = "test/configs";

pub fn config_path() -> &'static str {
    if cfg!(feature = "staging_env") {
        CONFIG_PATH_STAGING
    } else {
        CONFIG_PATH
    }
}