### Minekeeper ###

Minekeeper is a project to help make it easier to manage minecraft servers.
The end goal is to expose a web API to help with managing multiple minecraft instances on one server.

This project is very much under active development. Everything is subject to change.

### Current Features ###

* Start minecraft servers when minekeeper starts.
* API endpoints for starting, stopping, and reporting who is logged in.
* A happy welcome message when players login.

### Build instructions ###
* Rust 1.0 or later and Cargo
* Download the repo and use cargo build
* Installation is entirely manual right now, instructions coming soon.